<?php

namespace App\EventSubscriber;

use App\Controller\BaseController;
use App\Entity\AccessToken;
use App\ErrorHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Yaml\Yaml;

class RequestSubscriber extends BaseController implements EventSubscriberInterface
{

	private Request $request;
	private $routesConfig;
	private $needPermission = null;

	public function onKernelRequest(RequestEvent $event)
	{
		if (!$event->isMasterRequest()) {
			return;
		}
		$this->setRoutesConfig();

		$this->request = $event->getRequest();
		$res = null;
		$routeName = $this->request->attributes->get('_route');
		$requestContentType = $this->request->getContentType();

		if (!$this->hasInConfig($routeName)) return;

		$needRequestContentType = $this->routesConfig['routes'][$routeName]['content_type'];

		if ($this->needPermission($routeName)) {
			$this->setNeedPermission($this->routesConfig['routes'][$routeName]['permission']);
		}

		if ($needRequestContentType !== $requestContentType) {
			return ErrorHelper::notValidRequestContentType($needRequestContentType);
		}

		if ($this->needAccessToken($routeName)) {
			$res = $this->accessTokenMiddleware();
		}

		if (is_null($res)) {
			$event->setResponse($this->json(['success' => false, 'message' => 'server error'], 500));
			return;
		}

		if (!$res['success']) {
			$event->setResponse($this->json($res));
		}
	}

	private function hasInConfig($routeName): bool
	{
		return key_exists($routeName, $this->routesConfig['routes']);
	}

	private function needPermission($routeName): bool
	{
		return $this->routesConfig['routes'][$routeName]['permission'] !== null;
	}

	private function needAccessToken($routeName)
	{
		return $this->routesConfig['routes'][$routeName]['token_need'];
	}


	public function accessTokenMiddleware()
	{

		$token = $this->request->headers->get('token');

		if (!$token || $token === '')
			return ErrorHelper::authorizationFailed(ErrorHelper::AUTH_FAILED_TOKEN);

		/** @var AccessToken $accessToken */
		$accessToken = $this
			->getDoctrine()
			->getRepository(AccessToken::class)
			->findOneBy(['value' => $token]);


		if (!$accessToken)
			return ErrorHelper::authorizationFailed(ErrorHelper::AUTH_FAILED_TOKEN_NOT_FOUND);

		if ($this->needPermission !== null && ($accessToken->getMask() & $this->needPermission) != $this->needPermission)
			return ErrorHelper::authorizationFailed(ErrorHelper::AUTH_FAILED_NOT_PERMISSION);

		return [
			'success' => true,
			'message' => $this->routesConfig,
		];

	}

	public static function getSubscribedEvents()
	{
		return [
			'kernel.request' => 'onKernelRequest',
		];
	}


	public function setNeedPermission($needPermission): void
	{
		$this->needPermission = $needPermission;
	}


	public function setRoutesConfig(): void
	{
		$this->routesConfig = Yaml::parseFile(dirname(__DIR__, 2) . '/config/routes_conf.yaml');
	}
}
