<?php

namespace App\Entity;

use App\Repository\AccessTokenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccessTokenRepository::class)
 */
class AccessToken
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tokens")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $owner;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $value;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $mask;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $created;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $expired;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getOwner(): ?User
	{
		return $this->owner;
	}

	public function setOwner(?User $owner): self
	{
		$this->owner = $owner;

		return $this;
	}

	public function getValue(): ?string
	{
		return $this->value;
	}

	public function setValue(string $value): self
	{
		$this->value = $value;

		return $this;
	}

	public function getMask(): ?int
	{
		return $this->mask;
	}

	public function setMask(int $mask): self
	{
		$this->mask = $mask;

		return $this;
	}

	public function getCreated(): ?int
	{
		return $this->created;
	}

	public function setCreated(int $created): self
	{
		$this->created = $created;

		return $this;
	}

	public function getExpired(): ?int
	{
		return $this->expired;
	}

	public function setExpired(int $expired): self
	{
		$this->expired = $expired;

		return $this;
	}

	public function generate()
	{
		try {
			$token = bin2hex(random_bytes(20));
		} catch (\Exception $e) {
			return false;
		}

		$this->value = $token;

		return $this;
	}
}
