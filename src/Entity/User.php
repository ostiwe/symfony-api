<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="users")
 */
class User
{

	/* Битовые маски доступные пользователю */
	const CAN_READ = 1 << 1;
	const CAN_CREATE_POST = 1 << 2;
	const CAN_UPLOAD_FILES = 1 << 3;
	const CAN_CREATE_COMMENT = 1 << 4;
	const CAN_DELETE_COMMENT = 1 << 5;
	const CAN_WRITE_MESSAGES = 1 << 6;

	const USER_BLOCKED = 1 << 0;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $login;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $first_name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $last_name;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $mask;

	/**
	 * @ORM\OneToMany(targetEntity=AccessToken::class, mappedBy="owner", orphanRemoval=true)
	 */
	private $tokens;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $password;

	/**
	 * @ORM\OneToMany(targetEntity=Post::class, mappedBy="creator")
	 */
	private $createdPost;

	public function __construct()
	{
		$this->tokens = new ArrayCollection();
		$this->createdPost = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getLogin(): ?string
	{
		return $this->login;
	}

	public function setLogin(string $login): self
	{
		$this->login = $login;

		return $this;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	public function getFirstName(): ?string
	{
		return $this->first_name;
	}

	public function setFirstName(?string $first_name): self
	{
		$this->first_name = $first_name;

		return $this;
	}

	public function getLastName(): ?string
	{
		return $this->last_name;
	}

	public function setLastName(?string $last_name): self
	{
		$this->last_name = $last_name;

		return $this;
	}

	public function getMask(): ?int
	{
		return $this->mask;
	}

	public function setMask(int $mask): self
	{
		$this->mask = $mask;

		return $this;
	}

	/**
	 * @return Collection|AccessToken[]
	 */
	public function getTokens(): Collection
	{
		return $this->tokens;
	}

	public function addToken(AccessToken $token): self
	{
		if (!$this->tokens->contains($token)) {
			$this->tokens[] = $token;
			$token->setOwner($this);
		}

		return $this;
	}

	public function removeToken(AccessToken $token): self
	{
		if ($this->tokens->contains($token)) {
			$this->tokens->removeElement($token);
			// set the owning side to null (unless already changed)
			if ($token->getOwner() === $this) {
				$token->setOwner(null);
			}
		}

		return $this;
	}

	public function getPassword(): ?string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;

		return $this;
	}

	public function export()
	{
		return [
			'id' => $this->getId(),
			'login' => $this->getLogin(),
			'first_name' => $this->getFirstName(),
			'last_name' => $this->getLastName(),
			'mask' => $this->getMask(),
		];

	}

	/**
	 * @return Collection|Post[]
	 */
	public function getCreatedPost(): Collection
	{
		return $this->createdPost;
	}

	public function addCreatedPost(Post $createdPost): self
	{
		if (!$this->createdPost->contains($createdPost)) {
			$this->createdPost[] = $createdPost;
			$createdPost->setCreator($this);
		}

		return $this;
	}

	public function removeCreatedPost(Post $createdPost): self
	{
		if ($this->createdPost->contains($createdPost)) {
			$this->createdPost->removeElement($createdPost);
			// set the owning side to null (unless already changed)
			if ($createdPost->getCreator() === $this) {
				$createdPost->setCreator(null);
			}
		}

		return $this;
	}
}
