<?php

namespace App\Controller;


use App\Entity\AccessToken;
use App\Entity\User;
use App\ErrorHelper;
use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends BaseController
{

	/** @Route("/") */
	public function index()
	{
		return new Response(rand());
	}


	/**
	 * @Route("/users/create")
	 */
	public function createUser()
	{
		$entityManager = $this->getDoctrine()->getManager();
		$faker = Factory::create();
		for ($i = 0; $i < 20; $i++) {
			$user = new User();

			$user
				->setPassword(password_hash('1234', PASSWORD_DEFAULT))
				->setMask(999)
				->setEmail($faker->email)
				->setLogin($faker->userName)
				->setFirstName($faker->firstName);

			$entityManager->persist($user);
		}
		$entityManager->flush();

		return new Response("Ok, new users created");
	}

	/** @Route("/users/{page}") */
	public function getUsers($page = 1)
	{
		$users = $this->getDoctrine()->getRepository(User::class)->paginate((int)$page);
		return $this->json($users);
	}

	/** @Route("/token/create") */
	public function createToken()
	{

		$user = $this->getDoctrine()->getRepository(User::class)->find(1);
		$token = (new AccessToken())
			->setOwner($user)
			->setCreated(time())
			->setExpired(time() + 3600)
			->setMask(25)
			->generate();
		if (!$token) return $this->json(ErrorHelper::accessTokenGenerateError());

		$this->getDoctrine()->getManager()->persist($token);
		$this->getDoctrine()->getManager()->flush();


		return $this->json(['success' => true, 'message' => "access_token has id {$token->getId()}"]);
	}

	/** @Route("/users/{userId}/token") */
	public function createTokenForUser($userId)
	{
		$user = $this->getDoctrine()->getRepository(User::class)->find($userId);

		if (!$user) return $this->json(ErrorHelper::userNotFound());

		$newToken = new AccessToken();
		$token = $newToken
			->setOwner($user)
			->setCreated(time())
			->setExpired(time() + 3600)
			->setMask($user->getMask())
			->generate();
		if (!$token) return $this->json(ErrorHelper::accessTokenGenerateError());

		$user->addToken($newToken);

		$this->getDoctrine()->getManager()->persist($token);
		$this->getDoctrine()->getManager()->flush();


		return $this->json([
			'success' => true,
			'data' => [
				'access_token' => $token->getValue(),
				'expired_at' => $token->getExpired(),
			],
		]);

	}

}