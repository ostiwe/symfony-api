<?php


namespace App;


class ErrorHelper
{

	const NOT_VALID_REQUEST_CONTENT_TYPE = 1;
	const AUTH_FAILED_TOKEN = 2;
	const AUTH_FAILED_PASSWORD = 3;
	const AUTH_FAILED_TOKEN_NOT_FOUND = 4;
	const AUTH_FAILED_NOT_PERMISSION = 5;
	const USER_NOT_FOUND = 6;
	const ACCESS_TOKEN_GENERATE_ERROR = 7;

	public static function notValidRequestContentType(string $needType): array
	{
		return [
			'success' => false,
			'code' => self::NOT_VALID_REQUEST_CONTENT_TYPE,
			'message' => "request content-type must be a $needType",
		];
	}

	public static function authorizationFailed(int $authorizationType): array
	{
		return [
			'success' => false,
			'code' => $authorizationType,
			'message' => 'authorization failed',
		];
	}

	public static function userNotFound(): array
	{
		return [
			'success' => false,
			'code' => self::USER_NOT_FOUND,
			'message' => 'user not found',
		];
	}

	public static function accessTokenGenerateError(): array
	{
		return [
			'success' => false,
			'code' => self::ACCESS_TOKEN_GENERATE_ERROR,
			'message' => 'unable to create access token, try again later',
		];
	}

}